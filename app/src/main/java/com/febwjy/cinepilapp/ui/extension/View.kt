package com.febwjy.cinepilapp.ui.extension

import android.view.View

/**
 * Created by Febby Wijaya on 16/09/23.
 */
fun View.gone(){
    visibility = View.GONE
}

fun View.visible(){
    visibility = View.VISIBLE
}

fun View.invisible(){
    visibility = View.INVISIBLE
}